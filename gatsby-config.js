module.exports = {
  siteMetadata: {
    title: `Souvik Banerjee's Website`,
    description: `Hi, I am Souvik and this is my website.`,
    author: `@banerjeesouvik`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
  ],
}
