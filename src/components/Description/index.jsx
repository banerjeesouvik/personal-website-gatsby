import React from 'react'
import './style.css'

const Descreption = ({children}) => <span className='description'>{children}</span>

export default Descreption
